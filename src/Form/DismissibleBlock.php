<?php

namespace Drupal\dismissible_block\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines our form class.
 */
class DismissibleBlock extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'dismissible_block.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    // Get form config.
    $config = $this->config('dismissible_block.settings');

    // Set our count. Used in for loop below.
    if (!empty($form_state->getValue(['dismissible_block_fieldset', 'field_count']))) {

      // Check if $form_state has count value.
      $defaultCount = $form_state->getValue(['dismissible_block_fieldset', 'field_count']);
    }
    else {

      // Get config count value.
      $defaultCount = $config->get('field_count') ?: 1;
    }

    // Allow multiple field items.
    $form['#tree'] = TRUE;

    // Fieldset item.
    $form['dismissible_block_fieldset'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Field Options'),
      '#prefix' => '<div id="dismissible-block-fiedlset-wrapper">',
      '#suffix' => '</div>',
    ];

    // Field count item.
    $form['dismissible_block_fieldset']['field_count'] = [
      '#type' => 'number',
      '#title' => $this->t('Number of Fields'),
      '#required' => TRUE,
      '#description' => '<p>' . $this->t('Provide the number of field items below.') . '</p>',
      '#default_value' => $config->get('field_count') ?: 1,
    ];

    // Rebuild submit item.
    $form['dismissible_block_fieldset']['rebuild'] = [
      '#type' => 'submit',
      '#value' => $this->t('Apply number'),
      '#submit' => ['::rebuildFormSubmit'],
      '#ajax' => [
        'callback' => '::rebuildCountCallback',
        'wrapper' => 'dismissible-block-fiedlset-wrapper',
      ],
    ];

    $form['dismissible_block_fieldset']['spacer'] = [
      '#type' => 'item',
      '#markup' => $this->t('<br /><hr /><br />'),
    ];

    // Loop through our count and create form items.
    for ($i = 0; $i < $defaultCount; $i++) {

      $form['dismissible_block_fieldset']['block_id'][$i] = [
        '#type' => 'textfield',
        '#title' => $this->t('Block ID'),
        '#required' => FALSE,
        '#description' => '<p>' . $this->t('The rendered block ID while inspecting element.') . '</p>',
        '#default_value' => isset($config->get('block_id')[$i]) ? $config->get('block_id')[$i] : '',
      ];

    }

    return parent::buildForm($form, $form_state);

  }

  /**
   * {@inheritdoc}
   */
  public function rebuildCountCallback(array &$form, FormStateInterface $form_state) {
    return $form['dismissible_block_fieldset'];
  }

  /**
   * {@inheritdoc}
   */
  public function rebuildFormSubmit(array &$form, FormStateInterface $form_state) {
    $form_state->setRebuild(TRUE);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $values = $form_state->getValues();

    $this->configFactory->getEditable('dismissible_block.settings')
      ->set('field_count', $values['dismissible_block_fieldset']['field_count'])
      ->set('block_id', $values['dismissible_block_fieldset']['block_id'])
      ->save();

    parent::submitForm($form, $form_state);

  }

}
