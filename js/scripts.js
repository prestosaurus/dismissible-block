(function dismissibleBlock () {
  "use strict";

  var localStorageOn = storageAvailable('localStorage') ? true : false,
    observer;

  observer = new MutationObserver(function() {
    if (document.querySelectorAll(".dismissible-block")) {
      dismissibleReady();
      observer.disconnect();
    }
  });
  observer.observe(document.body, {
      childList: true,
      subtree: true
  });

  function dismissibleReady () {
    var dismissibleBlocks = document.querySelectorAll(".dismissible-block"),
      disStorageSetting,
      disDismissButton;

    // Loop through each dismiss block.
    dismissibleBlocks.forEach(function disEach (disBlock) {

      // Check if localStorage is enabled.
      if (localStorageOn) {

        // Check for block storage value.
        disStorageSetting = localStorage.getItem("dismiss-block-" + disBlock.id);

        // Hide block if localStorage value found.
        if (disStorageSetting !== null) {
          disBlock.classList.add("has-js-hide");
        }
        else {

          // Add show button class class.
          disBlock.classList.add("show-dismiss-btn");

          // Get the dismiss button.
          disDismissButton = disBlock.querySelector(".dismiss-block-btn");

          // Add button click listener.
          disDismissButton.addEventListener("click", disDismissButtonClick);

          // Hide block and set localStorage value.
          function disDismissButtonClick () {
            disBlock.classList.add("has-js-hide");
            localStorage.setItem("dismiss-block-" + disBlock.id, "dismissed");
          }
        }
      }

      // Remove all pre-hide classes independent of localStorage.
      disBlock.classList.remove("has-pre-js-hide");
    });
  };

  function storageAvailable(type) {
    try {
      var storage = window[type],
        x = '__storage_test__';
      storage.setItem(x, x);
      storage.removeItem(x);
      return true;
    }
    catch(e) {
      return false;
    }
  }

}());
